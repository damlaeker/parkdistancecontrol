﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;


namespace arduino
{
    public partial class Form1 : Form
    {

        public Form1()

        {
            
            InitializeComponent();
            serialPort1.Open();
            serialPort1.DataReceived += serialPort1_DataReceived;
          


            
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

           

           
        }

       
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen) 
                serialPort1.Close();
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            string POT = serialPort1.ReadLine();
            this.BeginInvoke(new LineReceivedEvent(LineReceived), POT);
        }
        private delegate void LineReceivedEvent(string POT);

        private void LineReceived(string POT)
        {

            int dis, dis2;

            label1.Text = POT;
            string[] numbers = Regex.Split(POT, @"\D+");
            foreach (string value in numbers)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    int i = int.Parse(value);
                }
            }
            label1.Text = numbers[0];
            label2.Text = numbers[1];

            if (Int32.TryParse(numbers[0], out dis))
            {
                dis = int.Parse(numbers[0]);
            }
            if (Int32.TryParse(numbers[1], out dis2))
            {
                dis2 = int.Parse(numbers[1]);
            }

            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            button3.TabStop = false;
            button3.FlatStyle = FlatStyle.Flat;
            button3.FlatAppearance.BorderSize = 0;
            button4.TabStop = false;
            button4.FlatStyle = FlatStyle.Flat;
            button4.FlatAppearance.BorderSize = 0;
            button5.TabStop = false;
            button5.FlatStyle = FlatStyle.Flat;
            button5.FlatAppearance.BorderSize = 0;
            button6.TabStop = false;
            button6.FlatStyle = FlatStyle.Flat;
            button6.FlatAppearance.BorderSize = 0;
            button7.TabStop = false;
            button7.FlatStyle = FlatStyle.Flat;
            button7.FlatAppearance.BorderSize = 0;
            button8.TabStop = false;
            button8.FlatStyle = FlatStyle.Flat;
            button8.FlatAppearance.BorderSize = 0;
            button9.TabStop = false;
            button9.FlatStyle = FlatStyle.Flat;
            button9.FlatAppearance.BorderSize = 0;
            button10.TabStop = false;
            button10.FlatStyle = FlatStyle.Flat;
            button10.FlatAppearance.BorderSize = 0;
            button11.TabStop = false;
            button11.FlatStyle = FlatStyle.Flat;
            button11.FlatAppearance.BorderSize = 0;
            
            button12.TabStop = false;
            button12.FlatStyle = FlatStyle.Flat;
            button12.FlatAppearance.BorderSize = 0;


            /* if (dis > 5 && dis <= 10) { button2.Visible = true; } else button2.Visible = false;
             if (dis > 0 && dis <= 5) { button1.Visible = true; } else button1.Visible = false;
             if (dis > 10 && dis <= 15) { button3.Visible = true; } else button3.Visible = false;*/

           
            if (dis>=0 && dis <= 5)
            {
                button1.BackColor = Color.Red;
                button2.BackColor = Color.White;
                button3.BackColor = Color.White;
                button4.BackColor = Color.White;
                button5.BackColor = Color.White;
                button6.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;


            }

            else  if (dis>5 && dis <= 10)
            {
                button2.BackColor = Color.Khaki;
                button1.BackColor = Color.White;
                button3.BackColor = Color.White;
                button4.BackColor = Color.White;
                button5.BackColor = Color.White;
                button6.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;




            }


            else if (dis >10 && dis <= 15)
            {

                button3.BackColor = Color.Yellow;
                button2.BackColor = Color.Khaki;
                button1.BackColor = Color.White;
                button4.BackColor = Color.White;
                button5.BackColor = Color.White;
                button6.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;

            }

            else if (dis > 15 && dis <= 20)
            {

                button4.BackColor = Color.PaleGreen;
                button2.BackColor = Color.White;
                button3.BackColor = Color.Yellow;
                button1.BackColor = Color.White;
                button5.BackColor = Color.White;
                button6.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;

            }

            else if (dis > 20 && dis <= 25)
            {

                button5.BackColor = Color.LimeGreen;
                button2.BackColor = Color.White;
                button3.BackColor = Color.White;
                button4.BackColor = Color.PaleGreen;
                button1.BackColor = Color.White;
                button6.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;

            }

            else if (dis > 30 && dis < 100)
            {

                button6.BackColor = Color.ForestGreen;
                button2.BackColor = Color.White;
                button3.BackColor = Color.White;
                button4.BackColor = Color.White;
                button5.BackColor = Color.LimeGreen;
                button1.BackColor = Color.White;
                button7.BackColor = Color.White;
                button8.BackColor = Color.White;
                button9.BackColor = Color.White;
                button10.BackColor = Color.White;
                button11.BackColor = Color.White;
                button12.BackColor = Color.White;

            }

            


               if (dis2 >= 0 && dis2 <= 5)
                {
                    button7.BackColor = Color.Red;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button1.BackColor = Color.White;
                    button8.BackColor = Color.White;
                    button9.BackColor = Color.White;
                    button10.BackColor = Color.White;
                    button11.BackColor = Color.White;
                    button12.BackColor = Color.White;

                }



                else if (dis2 > 5 && dis2 <= 10)
                {
                    button8.BackColor = Color.Khaki;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button7.BackColor = Color.White;
                    button1.BackColor = Color.White;
                    button9.BackColor = Color.White;
                    button10.BackColor = Color.White;
                    button11.BackColor = Color.White;
                    button12.BackColor = Color.White;


                }


                else if (dis2 > 10 && dis2 <= 15)
                {

                    button9.BackColor = Color.Yellow;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button7.BackColor = Color.White;
                    button8.BackColor = Color.Khaki;
                    button1.BackColor = Color.White;
                    button10.BackColor = Color.White;
                    button11.BackColor = Color.White;
                    button12.BackColor = Color.White;

                }

                else if (dis2 > 15 && dis2 <= 20)
                {

                    button10.BackColor = Color.PaleGreen;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button7.BackColor = Color.White;
                    button8.BackColor = Color.White;
                    button9.BackColor = Color.Khaki;
                    button1.BackColor = Color.White;
                    button11.BackColor = Color.White;
                    button12.BackColor = Color.White;

                }

                else if (dis2 > 20 && dis2 <= 25)
                {

                    button11.BackColor = Color.LimeGreen;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button7.BackColor = Color.White;
                    button8.BackColor = Color.White;
                    button9.BackColor = Color.White;
                    button10.BackColor = Color.PaleGreen;
                    button1.BackColor = Color.White;
                    button12.BackColor = Color.White;

                }

                else if (dis2 > 25 && dis2<100)
                {


                    button12.BackColor = Color.ForestGreen;
                    button2.BackColor = Color.White;
                    button3.BackColor = Color.White;
                    button4.BackColor = Color.White;
                    button5.BackColor = Color.White;
                    button6.BackColor = Color.White;
                    button7.BackColor = Color.White;
                    button8.BackColor = Color.White;
                    button9.BackColor = Color.White;
                    button10.BackColor = Color.White;
                    button11.BackColor = Color.LimeGreen;
                    button1.BackColor = Color.White;


                }
                if (button12.BackColor != Color.ForestGreen) { button12.Visible = false; } else button12.Visible = true;


                if (button1.BackColor != Color.Red) { button1.Visible = false; } else button1.Visible = true;
                if (button2.BackColor != Color.Khaki) { button2.Visible = false; } else button2.Visible = true;
                if (button3.BackColor != Color.Yellow) { button3.Visible = false; } else button3.Visible = true;
                if (button4.BackColor != Color.PaleGreen) { button4.Visible = false; } else button4.Visible = true;
                if (button5.BackColor != Color.LimeGreen) { button5.Visible = false; } else button5.Visible = true;
                if (button6.BackColor != Color.ForestGreen) { button6.Visible = false; } else button6.Visible = true;
                if (button7.BackColor != Color.Red) { button7.Visible = false; } else button7.Visible = true;
                if (button8.BackColor != Color.Khaki) { button8.Visible = false; } else button8.Visible = true;
                if (button9.BackColor != Color.Yellow) { button9.Visible = false; } else button9.Visible = true;
                if (button10.BackColor != Color.PaleGreen) { button10.Visible = false; } else button10.Visible = true;
                if (button11.BackColor != Color.LimeGreen) { button11.Visible = false; } else button11.Visible = true;


            }



        
        


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

     
       
    }
}
